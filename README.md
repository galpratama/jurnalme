# Introduction
Hello World! You can download and use this freely, but you can't sell or modify it. <br>
If you want to modify, feel free to contact me or you can submit a pull request 

# Login Credentials
Username : admin@admin.com <br>
Password : admin

# How to Install
1. Upload to your server using FTP client or copy to your localhost server
2. Change `jurnalme:8888` line with your url
  - **/assets/css/app.css** line 12 and 18
  - **/assets/css/frontpage.css** line 5 and 105
  - **/assets/js/tinymce/plugins/jbimages/dialog-v4.htm** line 11
  - **/assets/js/tinymce/plugins/jbimages/dialog.htm** line 13
  - **/nbproject/private/private.properties** line 7
